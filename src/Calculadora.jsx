import { Row, Button, Container } from 'react-bootstrap';
import './calculadora.css';

function Calculadora() {

    return (
        <div className="box">
        <Container className="p-4 mt-4">
            <Row>
                <div className="col-12">
                    <input dir="rtl" type="text" class="form-control lg" ></input>
                </div>
            </Row>
            <Row>
                <div className="col-3"><Button variant="outline-secondary" size="lg">1</Button></div>
                <div className="col-3"><Button variant="outline-secondary" size="lg">2</Button></div>
                <div className="col-3"><Button variant="outline-secondary" size="lg">3</Button></div>
                <div className="col-3"><Button variant="outline-secondary" size="lg">*</Button></div>
            </Row>
            <Row>
                <div className="col-3"><Button variant="outline-secondary" size="lg">4</Button></div>
                <div className="col-3"><Button variant="outline-secondary" size="lg">5</Button></div>
                <div className="col-3"><Button variant="outline-secondary" size="lg">6</Button></div>
                <div className="col-3"><Button variant="outline-secondary" size="lg">/</Button></div>
            </Row>
            <Row>
                <div className="col-3"><Button variant="outline-secondary" size="lg">7</Button></div>
                <div className="col-3"><Button variant="outline-secondary" size="lg">8</Button></div>
                <div className="col-3"><Button variant="outline-secondary" size="lg">9</Button></div>
                <div className="col-3"><Button variant="outline-secondary" size="lg"> - </Button></div>
            </Row>
            <Row>
                <div className="col-3"><Button variant="outline-secondary" size="lg">0</Button></div>
                <div className="col-3"><Button variant="outline-secondary" size="lg">C</Button></div>
                <div className="col-3"><Button variant="outline-secondary" size="lg">=</Button></div>
                <div className="col-3"><Button variant="outline-secondary" size="lg">+</Button></div>
            </Row>
        </Container>
        </div>
    )
}

export default Calculadora